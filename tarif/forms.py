from django import forms


class UploadPLForm(forms.Form):
    BREAKS_TYPE = (('manual', 'Manuel'), ('never', 'Jamais'), ('always', 'Toujours'))
    uploaded_file = forms.FileField(label='Sélectionner un fichier',
                                    help_text='max. 20 megabytes', required=True,
                                    widget=forms.FileInput(attrs={'class':'btn btn-secondary btn-sm pt-0 pb-0'})
                                    )
    debug_breaks = forms.BooleanField(required=False)
    fiche_degustation = forms.BooleanField(required=False)
    pagebreaks = forms.ChoiceField(label='Sauts de page', choices=BREAKS_TYPE, widget=forms.RadioSelect)
    manual_breaks = forms.CharField(label="Sauts manuels", required=False, max_length=100,
                                    help_text='ex: "12,42,17"')


class PdfGenerateForm(forms.Form):
    TYPE = (('tarif', "Tarif papier ou électronique"),
            ("reservation", "Réservation (électronique)"),
            ('degustation', 'Fiches de dégustation'))
    type_pdf = forms.ChoiceField(choices=TYPE, widget=forms.Select(attrs={'class':'btn btn-secondary btn-sm pt-0 pb-0'}))
    page_de_garde_custom = forms.BooleanField(required=False)
    occasion = forms.CharField(max_length=100, help_text='ex: "Automne 2019, 15 Avril 2018')
