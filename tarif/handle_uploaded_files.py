# coding: utf8

from django.utils.encoding import smart_str
import logging
import os.path
from . import generate_latex

logger = logging.getLogger(__name__)


def handle_file(f, filename='filename', path='/tmp/', parameters_dict=None):
    filename_utf8 = smart_str(filename)
    logger.debug(filename)
    logger.debug(filename_utf8)
    try:
        with open(path + filename_utf8, 'wb+') as destination:
            for chunk in f.chunks():
                destination.write(chunk)
        extension = os.path.splitext(filename)[1]
        if 'xlsx' in extension:
            msg = f"L'extension extension {extension} n'est pas supportée. Seuls les fichiers XLS peuvent être utilisés."
            return False, msg, 1
        elif "xls" in extension:
            ret, msg, error_nbr, parameters_dict = generate_latex.launch(filename=filename, path=path,
                                                                         parameters_dict=parameters_dict)
            return ret, msg, error_nbr
        elif 'tex' in extension:
            msg = "TODO: copy tex file in latex directory to be processed."
            return False, msg, 1
        msg = f"L'extension extension {extension} n'est pas supportée. Seuls les fichiers XLS peuvent être utilisés."
        return False, msg, 1
    except UnicodeEncodeError as unicode_error:
        ret = False
        msg = "Merci de renommer le fichier sans accents.\n"

        msg += "Message complet: {} \n ".format(unicode_error.start, unicode_error.end, unicode_error.reason)
        msg += str(unicode_error.encoding)
        return ret, msg, 1
