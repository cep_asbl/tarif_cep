# coding: utf8
from django.shortcuts import render, redirect
from django.core.mail import send_mail
from django.utils import timezone
from django.contrib import messages

from .forms import UploadPLForm, PdfGenerateForm
from .handle_uploaded_files import handle_file
from .models import SentMail

import os
import glob
import sys
from django.conf import settings
from datetime import datetime, timedelta
import base64

PATH = getattr(settings, 'PATH', '/tmp/cep/')
try:
    os.makedirs(PATH + 'tarif/upload/', exist_ok=True)
except PermissionError:
    print("ERROR PERMISSIONS", file=sys.stdout)
    pass

# Ugly global variables
PATH_upload = os.path.join(PATH, 'tarif/upload/')
PATH_download = os.path.join(PATH, 'tarif/download/')
PATH_latex = os.path.join(PATH, 'tarif/latex/')

parameters_dict = {}


# print(PATH, file=sys.stdout)

def index(request):
    """
    Generate the price list
    :param request:
    :return: page
    """
    global parameters_dict
    context = {'title': 'Génération du tarif'}
    upload_pl = glob.glob(PATH_upload + "*.xlsx") + glob.glob(PATH_upload + "*.tex")
    context['documents_upload'] = [os.path.basename(x) for x in upload_pl]
    download_pl = glob.glob(PATH_download + "*.pdf")
    context['documents_download'] = [os.path.basename(x) for x in download_pl]
    context['show_mail_button'] = True
    if request.session.get('hidden_button'):
        context['show_mail_button'] = False
        del request.session['hidden_button']

    if request.method == 'POST':
        form = UploadPLForm(request.POST, request.FILES)
        if form.is_valid():
            file_name = request.FILES['uploaded_file'].name
            tasting_notes = form.cleaned_data['fiche_degustation']
            pagebreaks = form.cleaned_data['pagebreaks']
            manual_breaks = form.cleaned_data['manual_breaks']
            debug_breaks = form.cleaned_data['debug_breaks']
            try:
                breaks_list = list(map(int, manual_breaks.split(',')))
            except ValueError:
                breaks_list = []
            parameters = {'tasting_notes': tasting_notes, 'breaks': pagebreaks, 'breaks_list': breaks_list,
                          'debug_breaks': debug_breaks}
            parameters_dict = parameters
            ret, msg, error_nbr, = handle_file(request.FILES['uploaded_file'], filename=file_name,
                                               path=PATH_upload, parameters_dict=parameters)
            context['msg'] = msg
            context['section_title'] = "Upload réussi!"
            context['subtitle'] = "Résultat de la moulinette"
            context['form_tex'] = True
            if error_nbr > 0:
                context['section_title'] = "Erreurs possibles"
                context['error'] = "Il y a {} erreurs. Veuillez les corriger pour pouvoir générer le PDF.".format(
                    error_nbr)
                context['form_tex'] = False
            context['error_nbr'] = error_nbr
            if not ret:
                return render(request, 'tarif/msg.html', context)
            return render(request, 'tarif/success.html', context)
    else:
        form = UploadPLForm()
        context['form'] = form
    return render(request, 'tarif/import.html', context)


def pdf(request):
    """
    Generate the PDF
    :param request:
    :return:
    """
    context = {'title': 'Génération du PDF'}
    upload_pl = glob.glob(PATH_latex + "*.tex")
    context['documents_upload'] = [os.path.basename(x) for x in upload_pl]
    download_pl = glob.glob(PATH_download + "*.pdf")
    context['documents_download'] = [os.path.basename(x) for x in download_pl]
    context['show_mail_button'] = True
    if request.session.get('hidden_button'):
        context['show_mail_button'] = False
        del request.session['hidden_button']
    msg = ""
    if request.method == 'POST':
        form = PdfGenerateForm(request.POST)
        if form.is_valid():
            type_pdf = form.cleaned_data['type_pdf']
            occasion = form.cleaned_data['occasion']
            custom = form.cleaned_data['page_de_garde_custom']
            msg += write_latex(type_pdf=type_pdf, occasion=occasion, custom=custom)
            context['section_title'] = "Création du fichier tarif réussie"
            context['subtitle'] = "Résultat de la moulinette"
            msg += "Type de tarif: " + str(type_pdf) + '\n' + "Occasion: " + str(occasion) + '\n'
            context['msg'] = msg
            context['form_pdf'] = True
            # TODO redirect and get the PDF if the file has changed (delete the old PDF before starting)
            return render(request, 'tarif/success.html', context)

    else:
        data = {}
        try:
            fiche_degustation = parameters_dict['fiche_degustation']
            if fiche_degustation:
                data = {'type_pdf': 'degustation'}
        except KeyError:
            pass
        form = PdfGenerateForm(initial=data)
        context['form'] = form
    return render(request, 'tarif/pdf.html', context)


def send_pricelist_mail(request, from_path):
    # TIME_ZONE is europe/bruxelles in the settings
    now = timezone.now()
    after = now - timedelta(hours=2)
    last_mail = SentMail.objects.filter(sending_time__range=(after, now))
    if not last_mail:
        autorization = request.META.get('HTTP_AUTHORIZATION')
        username = 'Un utilisateur Inconnu'
        if autorization:
            value = autorization.replace('Basic ', '')
            cred = base64.b64decode(value).decode('ascii')
            username = cred.split(':')[0]
        subject = 'Le générateur de tarifs est hors ligne'
        body = f"{username} demande l'allumage du générateur de tarif pour https://cep.agayon.be :-)."
        send_mail(subject, body, 'postmaster-odoo@compagnonsducep.be', ['arnaud@agayon.be'], fail_silently=False)
        SentMail.objects.create()
        messages.success(request, "Demande envoyée")
    else:
        sending_time = last_mail.all()[0].sending_time
        mail_time = sending_time.strftime("%HH:%M")
        delta_time = now - sending_time
        next_call = timedelta(hours=2) - delta_time
        hours, rem = divmod(next_call.seconds, 3600)
        minutes, rem = divmod(rem, 60)
        minutes_str = '%02d' % minutes
        messages.warning(request, f"Un mail a déjà été envoyé à {mail_time}. Réessayez dans {hours}H:{minutes_str}.")
    if from_path == 'generator':
        redirect_template = 'tarif_index'
    else:
        redirect_template = 'tarif_pdf'
    request.session['hidden_button'] = True
    return redirect(redirect_template)

def write_latex(type_pdf=None, occasion=None, custom=None):
    data_latex = {'maketitle': "\\maketitle",
                  'headers': "\\fancyhf{} \n \\fancyfoot[C]{\\thepage}",

                  }
    template_path = os.path.join(PATH_latex, 'price_list_template.tex')
    prices_path = os.path.join(PATH_latex, 'prices.tex')
    price_list_path = os.path.join(PATH_latex, 'price_list.tex')

    try:
        with open(template_path, 'r', encoding="utf-8") as template_file:
            template = template_file.read()

        if not custom:
            # maketitle is commented by default
            template = template.replace("%|maketitle|", data_latex['maketitle'])
        if type_pdf != 'tarif':
            # The generated file has probably a custom title
            # the header are not set in the reservation
            template = template.replace("%|headers|", data_latex['headers'])
        if type_pdf == 'degustation':
            title = "Liste des vins en dégustation"
        elif type_pdf == "tarif":
            title = "Tarif Compagnons du CEP"
        elif type_pdf == "reservation":
            title = "Liste des vins en réservation"
        else:
            title = 'PROBLEM'
        template = template.replace("|date|", occasion)
        template = template.replace("|title|", title)

        with open(prices_path, 'r', encoding="utf-8") as prices_file:
            prices = prices_file.read()
        template = template.replace("%|prices|", prices)
        with open(price_list_path, 'w', encoding="utf-8") as price_list_file:
            price_list_file.write(template)

    except FileNotFoundError as not_found:
        filename = get_file_with_parents(not_found.filename)
        msg = "Fichier introuvable : {}.\n".format(filename)
        return msg

    return "Fichier source généré\n"

# https://www.saltycrane.com/blog/2011/12/how-get-filename-and-its-parent-directory-python/
def get_file_with_parents(filepath, levels=1):
    common = filepath
    for i in range(levels + 1):
        common = os.path.dirname(common)
    return os.path.relpath(filepath, common)
