import logging
import os

from price_list_generator import table_reader
from price_list_generator import latex_gen
from price_list_generator import rst_gen

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch = logging.StreamHandler()
ch.setFormatter(formatter)
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

def get_parameters(path=None, filename=None):
    price_list = None
    if path and filename:
        price_list = os.path.join(path, filename)
    params = {'price_list': price_list,
              'debug': 'DEBUG',
              'type': 'latex',  # rst or LaTeX

              }
    return params

def launch(filename="", path='', parameters_dict=None):
    params = get_parameters(path=path, filename=filename)
    logger = logging.getLogger('price_list_generator')
    logger.info("Start launcher")
    log_msg = ''
    error_gen = 0
    error_prices = 0
    error_nbr = 0
    ret = 0
    tr = table_reader.Table()
    # read table
    price_list, flat_list, log_msg_wine_reader, error_reader = tr.wines_reader(parameters=params)
    parent_path = os.path.abspath(os.path.join(path, os.pardir))
    latex_path = os.path.join(parent_path, 'latex')
    price_list_template_tex = os.path.join(path)
    if price_list and flat_list:
        sellers_dict = tr.seller_reader(filename=params['price_list'])
        if not parameters_dict:
            parameters_dict = tr.parameters_reader(filename=params['price_list'])
        regions_dict = tr.regions_reader(filename=params['price_list'])
        if params['type'].lower() == 'latex':
            # Generate latex files
            latex = latex_gen.Latex(price_list_template_tex=os.path.join(latex_path, 'price_list_template.tex'),
                                    tabular_template_tex=os.path.join(latex_path, 'tabular_template.tex'),
                                    prices_tex=os.path.join(latex_path, 'prices.tex'))
            # create tex tile
            ret = latex.gen_templates()
            if ret:
                error_nbr += 1
                log_msg += ret
                return False, log_msg, error_nbr, parameters_dict
            flat_list, log_msg_gen_wines, error_gen = latex.generate_wines_list(flat_list, sellers_dict,
                                                                                parameters_dict, regions_dict)
            log_msg_prices, error_prices = latex.write_prices()
            # compile tex file
            latex.pdflatex()
            log_msg += log_msg_wine_reader + log_msg_gen_wines + log_msg_prices
        elif params['type'].lower() == 'rst':
            rst_instance = rst_gen.Rst()
            rst_instance.gen_templates()
            rst_instance.generate_wines_list(flat_list, sellers_dict, parameters_dict, regions_dict)
            rst_instance.write_prices()
            rst_instance.rst2pdf()

    else:
        logger.critical("Check your Excel File")
    error_nbr = error_reader + error_gen + error_prices
    return True, log_msg, error_nbr, parameters_dict
