"""
WSGI config for odoo_cep project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/
"""

import os
import sys
import site

# Add the site-packages of the chosen virtualenv to work with
site.addsitedir('/home/dev/web/python/django/venv_cep/lib/python3.5/site-packages')  # ARJ TOTO CHANGE

# Add the app's directory to the PYTHONPATH
sys.path.append('/home/dev/web/python/django/cep/odoo_cep/')   # ARJ TOTO CHANGE
sys.path.append('/home/dev/web/python/django/cep/odoo_cep/odoo_cep')   # ARJ TOTO CHANGE

os.environ['DJANGO_SETTINGS_MODULE'] = 'tarif_cep.settings'
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "tarif_cep.settings")

from django.core.wsgi import get_wsgi_application
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "tarif_cep.settings")
application = get_wsgi_application()
