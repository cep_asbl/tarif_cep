from django.shortcuts import render

def index(request):
    context = {
        'title': 'Gestionnaire des Compagnons du CEP'
    }
    return render(request, 'home/index.html', context)
