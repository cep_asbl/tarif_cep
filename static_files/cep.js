/*
	Odoo_cep small js utils
	All right reserved. Compagnons du Cep: www.compagnonsducep.be
*/

(function() {
    console.info("Django CEP");
    if (document.getElementById('a_odoo')) {
        // This is the page interacting with odoo
        action_choice = document.getElementById('id_choix');
        let inputCodes = document.getElementById('id_codes_livraison').closest("tr");
        // default value is not to delete products
        inputCodes.style.display = "none";
        action_choice.addEventListener("change", ev => {
             let inputCodes = document.getElementById('id_codes_livraison').closest("tr");
             inputCodes.style.display = "none"
             if (ev.target.selectedOptions[0].value !== 'delete') {
                inputCodes.style.display = "none";
             } else {
                inputCodes.style.display = "revert";
             }
        });
    }
    if (document.getElementById('a_progress_container')) {
        // This is the page displaying the progress bar
        const progress = document.getElementById("a_progress_bar");
        const downloadButton = document.getElementById("a_pdf_download");
        const progressContainer = document.getElementById('a_progress_container');
        let count = 1;
        //const id = setInterval(frame, 675); // ~ 1'10 to generate the PDF with Errol
        const id = setInterval(frame, 25); // ~ 1'10 to generate 
        function frame() {
            if (count >= 100) {
                clearInterval(id);
                count = 0;
                downloadButton.style.display= 'block';
                progressContainer.style.display = 'none';
              } else {
                count++;
                progress.value = count;
                progress.textContent = count.toString() + '%';
              }
        }
    }
})();

